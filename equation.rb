class Equation
  class << self
    # Solve the Linear Equation ax + b = 0
    def linear_equation(a, b)
      raise RuntimeError.new("a: #{a} and b: #{b} must both be numbers.") if (!a.is_a?(Numeric) || !b.is_a?(Numeric))
      return -(b.to_f)/a if a != 0
      return 'x can be any value.' if b == 0
      'There is no value for x.'
    end

  end
end
