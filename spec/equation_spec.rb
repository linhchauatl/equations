require 'minitest/autorun'
require 'minitest-spec-context'
require 'minitest/reporters'
require_relative '../equation'

Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

describe Equation do
  context 'linear_equation' do
    it 'raises RuntimeError if a is not a number' do
      proc { Equation.linear_equation('a', 5) }.must_raise(RuntimeError, 'a: a and b: 5 must both be numbers.')
    end

    it 'raises RuntimeError if b is not a number' do
      proc { Equation.linear_equation(2, 'b') }.must_raise(RuntimeError, 'a: 2 and b: b must both be numbers.')
    end

    it "returns 'x can be any value.'  if both a and b are equal to 0" do
      Equation.linear_equation(0, 0).must_equal 'x can be any value.'
    end

    it "returns 'There is no value for x.'  if  a is equal to 0 and b is not 0" do
      Equation.linear_equation(0, 10).must_equal 'There is no value for x.'
    end

    it 'returns a correct value for x if both a and b are not 0' do
      Equation.linear_equation(2, 0).must_equal 0
      Equation.linear_equation(4, -20).must_equal 5
    end
  end
end